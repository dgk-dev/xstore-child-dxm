<?php

/** analytics */
require(get_stylesheet_directory().'/includes/analytics.php');

/** Cambios en tienda */
require(get_stylesheet_directory().'/includes/store-changes.php');

/** funciones de búsqueda */
require(get_stylesheet_directory().'/includes/search-results.php');

add_action('wp_enqueue_scripts', 'dxm_resources');
function dxm_resources(){
    etheme_child_styles();
    wp_register_script('main-functions', get_stylesheet_directory_uri() . '/js/footer-bundle.js', null, '1.0', true);
    // wp_localize_script('main-functions', 'mainGlobalObject', array(
	// 	'add_cart_button_script' => is_shop() || is_product_category() || is_product_tag(),
	// ));
    wp_enqueue_script('main-functions');
    if (is_front_page() || is_single() ) wp_dequeue_script('wc-cart-fragments');
}

//langs: themes and plugins
add_action( 'after_setup_theme', 'dxm_theme_lang' );
function dxm_theme_lang() {
    load_child_theme_textdomain( 'xstore', get_stylesheet_directory() . '/languages' );
    
    //plugins
    unload_textdomain('xstore-core');
    load_textdomain('xstore-core', get_stylesheet_directory() . '/languages/xstore-core-es_ES.mo');
}

/**
 * Reescribe etiquetas
 */
add_filter( 'gettext', 'dxm_change_translation_strings', 20, 3 );
function dxm_change_translation_strings( $translated_text, $text, $domain ) {
    switch ( $translated_text ) {
        case 'Guardar mi nombre, correo electrónico y web en este navegador para la próxima vez que comente.' :
            $translated_text = __( 'Guardar mi nombre y correo electrónico en este navegador para la próxima vez que comente.', $domain );
        break;
        case '(con impuestos)' :
            $translated_text = __( '(con IVA)', $domain );
        break;
        case 'Load More' :
            $translated_text = __( 'Cargar más', $domain );
        break;
        case 'Shipping to ' :
            $translated_text = __( 'Enviar a ', $domain );
        break;
        case 'Change address' :
            $translated_text = __( 'Cambiar dirección', $domain );
        break;
        
    }
    return $translated_text;
}

/**
 * Gestión de roles shop_orders = pedidos : dado de alta desde plugin
 */
add_action( 'wp_before_admin_bar_render', 'dxm_admin_bar_remove_logo', 0 );
function dxm_admin_bar_remove_logo() {
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu( 'wp-logo' );
}
add_action( 'admin_menu', 'dxm_remove_menu_pages', 999);
function dxm_remove_menu_pages() {
  global $current_user;
   
  $user_roles = $current_user->roles;
  $user_role = array_shift($user_roles);
  if($user_role == "shop_orders") {
    $remove_submenu = remove_submenu_page('woocommerce', 'wc-settings');
    $remove_submenu = remove_submenu_page('woocommerce', 'wc-addons');
    $remove_submenu = remove_submenu_page('woocommerce', 'wc-addons');
    $remove_submenu = remove_submenu_page('woocommerce', 'wc-addons');
    $remove_menu = remove_menu_page('woocommerce-marketing');
    $remove_menu = remove_menu_page('yith_plugin_panel');
  }
}

add_action('wp_footer', 'dxm_add_script_data');
function dxm_add_script_data(){
?>
	<script type="application/ld+json">
    {
        "@context" : "http://schema.org",
        "@type" : "Organization",
        "name" : "Dioxmin",
        "url" : "https://dioxmin.com.mx",
        "sameAs" : [
            "https://www.facebook.com/Dioxmin-Oficial-110993974016371/",
            "https://www.instagram.com/dioxmin_oficial/"
        ],
        "address": {
            "@type": "PostalAddress",
            "streetAddress": "Euler No. 152 Int. 201 Colonia Polanco V Sección",
            "addressRegion": "Del. Miguel Hidalgo",
            "postalCode": "11560",
            "addressCountry": "MX"
        }
    }
	</script>
<?php
}

// add_filter( 'woocommerce_available_payment_gateways', 'dxm_disable_openpay_test' );
  
function dxm_disable_openpay_test( $available_gateways ) {
    global $current_user;
    get_currentuserinfo();

    $email = (string) $current_user->user_email;
   if ( $email != 'ricardo@dgk.com.mx' ) {
      unset( $available_gateways['openpay_cards'] );
   } 
   return $available_gateways;
}