<?php
//Google analytics tag
add_action( 'wp_head', 'dxm_google_analytics', 7 );
function dxm_google_analytics() {
    ?>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-177835997-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-177835997-1');
    </script>
    <?php
}