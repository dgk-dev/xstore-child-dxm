<?php
//menú líneas de producto
add_filter('woocommerce_attribute_show_in_nav_menus','dxm_woocommerce_menu_elements', 1, 2);
function dxm_woocommerce_menu_elements( $register, $name = '' ) {
     if ( $name == 'product_cat' ) $register = true;
     return $register;
}

//mensaje extra en recuperar password
add_action('woocommerce_after_lost_password_confirmation_message', 'dxm_after_lost_password_confirmation_message');
function dxm_after_lost_password_confirmation_message(){
	echo "<p><strong>Si no lo encuentras en la bandeja de entrada, busca en tu bandeja de spam o correo no deseado.</strong></p>";
}


/**
 * RFC - Razón social
 */
// Add / Display additional billing fields in checkout and My account > Edit adresses > Billing form
add_filter( 'woocommerce_billing_fields', 'dxm_additional_billing_fields', 10, 1 );
function dxm_additional_billing_fields( $fields )
{
    $fields['billing_rfc'] = array(
        'type'        => 'text', // add field type
        'label'       => __('RFC', 'woocommerce'), // Add custom field label
        'placeholder' => _x('RFC', 'placeholder', 'woocommerce'), // Add custom field placeholder
        'required'    => false, // if field is required or not
        'clear'       => false, // add clear or not
        'class'       => array( 'form-row-first' ),
    );
    
    $fields['billing_razon_social'] = array(
        'type'        => 'text', // add field type
        'label'       => __('Razón social', 'woocommerce'), // Add custom field label
        'placeholder' => _x('Razón social', 'placeholder', 'woocommerce'), // Add custom field placeholder
        'required'    => false, // if field is required or not
        'clear'       => false, // add clear or not
        'class'       => array( 'form-row-first' ),

    );

    return  $fields;
}

add_filter( 'woocommerce_my_account_my_address_formatted_address', function( $args, $customer_id, $name ){
    if($name == 'billing'){
        $args['rfc'] = get_user_meta( $customer_id, $name . '_rfc', true );
        $args['razon_social'] = get_user_meta( $customer_id, $name . '_razon_social', true );
    }
    return $args;
}, 10, 3 ); 

// modify the address formats
add_filter( 'woocommerce_localisation_address_formats', function( $formats ){
    foreach ( $formats as $key => &$format ) {
        // put a break and then the phone after each format.
        $format .= "\n{rfc}";
        $format .= "\n{razon_social}";
    }
    return $formats;
} );

// add the replacement value
add_filter( 'woocommerce_formatted_address_replacements', function( $replacements, $args ){
    // we want to replace {rfc} in the format with the data we populated
    $replacements['{rfc}'] = isset($args['rfc']) ? $args['rfc'] : '';
    $replacements['{razon_social}'] = isset($args['razon_social']) ? $args['razon_social'] : '';
    return $replacements;
}, 10, 2 );

// Get the field values to be displayed in admin Order edit pages
add_filter('woocommerce_order_formatted_billing_address', 'dxm_add_woocommerce_order_fields', 10, 2);
function dxm_add_woocommerce_order_fields($address, $order ) {
    $address['rfc'] = get_post_meta( $order->get_id(), 'billing_rfc', true );
    $address['razon_social'] = get_post_meta( $order->get_id(), 'billing_razon_social', true );

    return $address;
}

// Make the custom billing field Editable in Admin order pages
add_filter('woocommerce_admin_billing_fields', 'dxm_add_woocommerce_admin_billing_fields');
function dxm_add_woocommerce_admin_billing_fields($billing_fields) {
    $billing_fields['rfc'] = array( 'label' => __('RFC', 'woocommerce') );
    $billing_fields['razon_social'] = array( 'label' => __('Razón social', 'woocommerce') );

    return $billing_fields;
}

/**Número de referencia en correo */
add_filter('woocommerce_email_before_order_table', 'dxm_email_add_order_reference_number', 10, 4);
function dxm_email_add_order_reference_number($order, $sent_to_admin, $plain_text, $email) {
    echo '<p style="font-size: 1.25em"><strong>Número de referencia de pedido:</strong> '.$order->get_id().'</p>';
}

/**Mostrar solo envio gratis si está disponible */
add_filter( 'woocommerce_package_rates', 'dxm_hide_shipping_when_free_is_available', 100 );
function dxm_hide_shipping_when_free_is_available( $rates ) {
	$free = array();
	foreach ( $rates as $rate_id => $rate ) {
		if ( 'free_shipping' === $rate->method_id ) {
			$free[ $rate_id ] = $rate;
			break;
		}
	}
	return ! empty( $free ) ? $free : $rates;
}

/**Casilla para requerir factura */
add_action( 'woocommerce_after_checkout_billing_form', 'dxm_add_send_billing_check' );
function dxm_add_send_billing_check( $checkout ) {
    
    woocommerce_form_field( 'send_billing', array(
        'type'	=> 'checkbox',
		'class'	=> array('form-row-wide'),
		'label'	=> '¿Deseas recibir factura?',
    ), $checkout->get_value( 'send_billing' ) );
    
}

add_action( 'woocommerce_checkout_update_order_meta', 'dxm_save_send_billing' );
function dxm_save_send_billing( $order_id ){ 
	if( !empty( $_POST['send_billing'] ) && $_POST['send_billing'] == 1 )
		update_post_meta( $order_id, 'send_billing', 1 );
}

add_filter( 'woocommerce_email_order_meta_fields', 'dxm_woocommerce_email_order_send_billing_field', 10, 3 );
function dxm_woocommerce_email_order_send_billing_field( $fields, $sent_to_admin, $order ) {
    $fields['send_billing'] = array(
        'label' => __( '¿Deseas recibir factura?' ),
        'value' => get_post_meta( $order->get_id(), 'send_billing', true ) ? 'Sí' : 'No',
    );
    return $fields;
}

add_action( 'woocommerce_admin_order_data_after_billing_address', 'dxm_display_send_billing_on_order_edit_pages', 10, 1 );
function dxm_display_send_billing_on_order_edit_pages( $order ){
    $send_billing = get_post_meta( $order->get_id(), 'send_billing', true );
    if( ! empty( $send_billing ) ){
        $billing_text = $send_billing ? 'Sí' : 'No';
        echo '<p><strong>¿Deseas recibir factura?: </strong>'.$billing_text.'</p>';
    }
}


// Display 'pickup html data' in "Order received" and "Order view" pages (frontend)
add_action( 'woocommerce_order_details_after_order_table', 'dxm_display_send_billing_data_in_orders', 10 );
function dxm_display_send_billing_data_in_orders( $order ) {
    $send_billing = get_post_meta( $order->get_id(), 'send_billing', true );
    if( ! empty( $send_billing ) ){
        $billing_text = $send_billing ? 'Sí' : 'No';
        echo '<p><strong>¿Deseas recibir factura?: </strong>'.$billing_text.'</p>';
    }
}


/**
 * Cambiar mensajes de stock
 */

add_filter( 'woocommerce_get_availability', 'dxm_change_stock_messages', 20, 2 );

function dxm_change_stock_messages( $availability, $_product ) {

    if ( $_product->is_in_stock() ) {
        if ( $_product->get_stock_quantity() <  $_product->get_low_stock_amount() ) {
            $availability['availability'] = __( "Quedan pocas", 'woocommerce' );
        }else{
            $availability['availability'] = __( "Hay stock", 'woocommerce' );
        }
    }
    return $availability;
}


/**
 * Desactivar wc_ajax fragments
 */

// add_action( 'wp_enqueue_scripts', 'dxm_disable_woocommerce_cart_fragments', 11 ); 
 
function dxm_disable_woocommerce_cart_fragments() { 
   wp_dequeue_script( 'wc-cart-fragments' ); 
}

add_action('woocommerce_thankyou', 'dxm_affiliate_order_email', 10);
function dxm_affiliate_order_email($order_id){
    if(get_post_meta($order_id, '_affiliate_mail_sent', true) || !function_exists('yit_get_prop')) return;
    
    $order = wc_get_order($order_id);
    $token = yit_get_prop( $order, '_yith_wcaf_referral');
    
    if(!$token) return;
    $affiliate = YITH_WCAF_Affiliate_Handler()->get_affiliate_by_token( $token );
    
    add_filter('woocommerce_new_order_email_allows_resend', 'dxm_allow_email_resend');
    $wc_email = WC()->mailer()->get_emails()['WC_Email_New_Order'];
    $wc_email->recipient = $affiliate['user_email'];
    $wc_email->settings['subject'] = '[Dioxmin] Se realizó una compra con tu referencia';
    $wc_email->trigger($order_id);
    remove_filter('woocommerce_new_order_email_allows_resend', 'dxm_allow_email_resend');
    update_post_meta($order_id, '_affiliate_mail_sent', 1);
}

function dxm_allow_email_resend(){
    return true;
}