(function ($) {
    var resizeTimer = false;

    $(window).on('resize', function(e) {
        clearTimeout(resizeTimer);
        resizeTimer = setTimeout(function() {
            var diff = $(window).outerHeight() - $('body').outerHeight();
            if(diff > 0) $('.page-wrapper').css('padding-bottom', diff+'px');
            resizeTimer = false;      
        }, 250);

    }).trigger('resize');
    
    $(document.body).on('updated_checkout', function(){
        $('#openpay-card-cvc').attr('type', 'password');
    });

})(jQuery);